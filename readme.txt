=== Kajuzi Candy ===
Contributors: kajuzi
Tags: page-builder, elementor, custom
Requires at least: 4.4
Tested up to: 5.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds post-grid and taxonomy card element to Elementor page builder. Built by modifying VOID Elementor Post Grid by Void Coders

== Description ==

Adds post-grid and taxonomy card element to Elementor page builder. Built by modifying VOID Elementor Post Grid by Void Coders


What Does This plugin give you?


== Installation ==

1. Upload the plugin folder after extracting it to the `/wp-content/plugins/(the folder of the extracted plugin)` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Go to elementor page builder mode and then select the newly add widget and you will see many options when you drag and drop the widget to the live page. To see the real preview in live mode you must click apply button on top to update the layout and finally hit save in elementor and visit your page to see the changes. 

