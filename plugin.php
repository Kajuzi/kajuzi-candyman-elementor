<?php
namespace kajuzigrid;  //main namespace
use kajuzigrid\Widgets\Kajuzi_Candyman_Elementor;   //path define same as class name of the widget
use kajuzigrid\Widgets\Candy_By_Taxonomy;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly



// Add a custom category for panel widgets
add_action( 'elementor/init', function() {
   \Elementor\Plugin::$instance->elements_manager->add_category( 
   	'kajuzi-candyman',                 // the name of the category
   	[
   		'title' => esc_html__( 'Kajuzi Candyman', 'kajuzi_candyman'),
   		'icon' => 'fa fa-header', //default icon
   	],
   	1 // position
   );
} );



/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Plugin {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	
	public function __construct() {
		$this->add_actions();
	}

	/**
	 * Add Actions
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function add_actions() {
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ] );

		add_action( 'elementor/frontend/after_register_scripts', function() {		
			wp_enqueue_style( 'void-grid-main', plugins_url ( '/assets/css/main.css', KAJUZI_CANDYMAN_FILE_ ),false,'1.0','all');
			wp_enqueue_style( 'void-grid-bootstrap', plugins_url ( '/assets/css/bootstrap.min.css', KAJUZI_CANDYMAN_FILE_ ),false,'3.3.7','all');
			//load equal height js
			wp_enqueue_script( 'void-grid-equal-height-js', plugins_url ( '/assets/js/jquery.matchHeight-min.js', KAJUZI_CANDYMAN_FILE_ ), array(), '3.3.7', true );
			//load custom js
			wp_enqueue_script( 'void-grid-custom-js', plugins_url ( '/assets/js/custom.js', KAJUZI_CANDYMAN_FILE_ ), array(), '1.0', true );
		} );

	}

	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_widgets_registered() {
		$this->includes();
		$this->register_widget();
	}

	/**
	 * Includes
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function includes() {
		$kajuzi_candyman_elementor= array_map('basename', glob(dirname( __FILE__ ) . '/widgets/*.php'));
		require KAJUZI_CANDYMAN_DIR . '/helper/helper.php';
		foreach($kajuzi_candyman_elementor as $key => $value){
   			require KAJUZI_CANDYMAN_DIR . '/widgets/'.$value;
		}
	}

	/**
	 * Register Widget
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function register_widget() {    
	//this is where we create objects for each widget the above  ->use kajuzigrid\Widgets\Hello_World; is needed

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Kajuzi_Candyman_Elementor() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Candy_By_Taxonomy() );
	}
}

new Plugin();
