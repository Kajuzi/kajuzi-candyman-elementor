<?php  
// kajuzigrid Shortcodes 

$col_no=$count=$col_width=$post_count='';
global $image_size;
function kajuzigrid_sc_post_grid( $atts ) {
    extract( shortcode_atts( array (
         'post_type'  => 'post',
         'filter_thumbnail' => 0,
         'taxonomy_type'    => '',
         'cat_exclude'      => '', // actually include or exclude both
         'terms'            => '', 
         'display_type'     => '1',   
         'items_to_show'    =>  8,
         'posts_per_row'    =>  2,         
         'image_style'      => '1',
         'orderby'          => 'date',
         'order'            => 'DESC', 
         'offset'           =>  0,  
         'sticky_ignore'    =>  0,
         'display_type'     => 'grid',
         'pagination_yes'   =>  1,
		     'image_size'       =>  ''
    ), $atts ));

   
  set_transient('void_grid_image_size', $image_size, '60' );
  global $col_no,$count,$col_width;
  
  $count = 0;         
  if ($posts_per_row==1)  { $col_width = 12; $col_no = 1; }
  else if ($posts_per_row==2){ $col_width = 6; $col_no = 2; }
  else if ($posts_per_row==3){ $col_width = 4; $col_no = 3; }
  else if ($posts_per_row==4){ $col_width = 3; $col_no = 4; }
  else if ($posts_per_row==6){ $col_width = 2; $col_no = 6; }
  else{ $col_width = 12; $col_no = 1; }
   
    if( $display_type == '1' ){
      $display_type = 'grid';
    }
    elseif( $display_type == '2'){
      $display_type = 'list';
    }
    elseif( $display_type == '3'){
      $display_type = 'first-post-grid';
    }
    elseif( $display_type == '4'){
      $display_type = 'first-post-list';
    }
    elseif( $display_type == '5'){
      $display_type = 'minimal';
    }
    
    if( !empty( $image_style ) ){
      if( $image_style == 1){
        $image_style = '';
      }
      elseif( $image_style == 2){
        $image_style = 'top-left';
      }
      else{
        $image_style = 'top-right';
      }
    }
    else{
      $image_style = '';
    }         
  if( !empty( $taxonomy_type ) ){
    $tax_query = array(                        
                    array(
                            'taxonomy' => $taxonomy_type,
                            'field'    => 'term_id',
                            'terms'    => explode( ',', $terms ),
                          ),
                    );
  }
  else{
    $tax_query = '';
  }

  if($filter_thumbnail){
    $void_image_condition = array(
      'meta_query' => array( 
          array(
              'key' => '_thumbnail_id',
              'compare' => $filter_thumbnail,
            ) 
        )
    );
  }else{
    $void_image_condition='';
  }


  $templates = new Void_Template_Loader;
  

  $grid_query= null;

if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // if is static front page
    $paged = get_query_var('page');
} else {
    $paged = 1;
}


    $args = array(
       'post_type'      => $post_type,
       'meta_query'     => $void_image_condition,
       'cat'            => $cat_exclude,        // actually include or exclude both  
       'post_status'    => 'publish',
       'items_to_show'  => $items_to_show, 
       'paged'          => $paged,   
       'tax_query'      => $tax_query,
       'orderby'        => $orderby,
       'order'          => $order,   //ASC / DESC
       'ignore_sticky_posts' => $sticky_ignore,
       'void_grid_query' => 'yes',
       'void_set_offset' => $offset,
  );

?>

<?php
wp_reset_postdata();
return ob_get_clean();
}

function kajuzi_pruduct_taxonomies( $atts ) {
    extract( shortcode_atts( array (
         'post_type'  => 'product',
         'filter_thumbnail' => 0,
         'taxonomy_type'    => '',
         'more_text'        => '',
         'more_link'        => '',
         'cat_exclude'      => '', // actually include or exclude both
         'terms'            => '', 
         'display_type'     => '1',   
         'items_to_show'    =>  8,
         'posts_per_row'    =>  2,         
         'image_style'      => '1',
         'orderby'          => 'date',
         'order'            => 'DESC', 
         'offset'           =>  0,  
         'sticky_ignore'    =>  0,
         'display_type'     => 'grid',
         'pagination_yes'   =>  1,
		     'image_size'       =>  ''
    ), $atts ));

  set_transient('void_grid_image_size', $image_size, '60' );
  global $col_no,$count,$col_width;
  
  $count = 0;         
  if ($posts_per_row==1)  { $col_width = 12; $col_no = 1; }
  else if ($posts_per_row==2){ $col_width = 6; $col_no = 2; }
  else if ($posts_per_row==3){ $col_width = 4; $col_no = 3; }
  else if ($posts_per_row==4){ $col_width = 3; $col_no = 4; }
  else if ($posts_per_row==6){ $col_width = 2; $col_no = 6; }
  else{ $col_width = 12; $col_no = 1; }
   
    $display_type = 'list';
    
    if( !empty( $image_style ) ){
      if( $image_style == 1){
        $image_style = '';
      }
      elseif( $image_style == 2){
        $image_style = 'top-left';
      }
      else{
        $image_style = 'top-right';
      }
    }
    else{
      $image_style = '';
    }         
  if( !empty( $taxonomy_type ) ){
    $tax_query = array(                        
                    array(
                            'taxonomy' => $taxonomy_type,
                            'field'    => 'term_id',
                            'terms'    => explode( ',', $terms ),
                          ),
                    );
  }
  else{
    $tax_query = '';
  }

  if($filter_thumbnail){
    $void_image_condition = array(
      'meta_query' => array( 
          array(
              'key' => '_thumbnail_id',
              'compare' => $filter_thumbnail,
            ) 
        )
    );
  }else{
    $void_image_condition='';
  }


  $templates = new Void_Template_Loader;
  

  $grid_query= null;

if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // if is static front page
    $paged = get_query_var('page');
} else {
    $paged = 1;
}


    $args = array(
       'post_type'      => $post_type,
       'meta_query'     => $void_image_condition,
       'cat'            => $cat_exclude,        // actually include or exclude both  
       'post_status'    => 'publish',
       'items_to_show'  => $posts, 
       'paged'          => $paged,   
       'tax_query'      => $tax_query,
       'orderby'        => $orderby,
       'order'          => $order,  
       'ignore_sticky_posts' => $sticky_posts,
       'void_grid_query' => 'yes',
       'void_set_offset' => $offset,
  );

if ( !empty( $terms ) ) {
  $list = get_term_children( $terms, $taxonomy_type );
  foreach ($list as $key => $term_id) {  
      $list[$key]  = get_term_by('id', $term_id, $taxonomy_type);
  }
} else {
  $list = get_terms(['taxonomy' => $taxonomy_type, 'hide_empty' => false, 'parent' => 0]);
}
?>

<div class="taxonomy-card taxonomy-grid">
  <div class="<?php echo esc_html( $display_type . ' '. $image_style); ?>" >       
    <div class="kajuzi-taxonomy-card-list">
        <ul>
          <?php 
            $upper_limit = $offset + $items_to_show - 1;
            for ($i=$offset; $i < $upper_limit; $i++) { 
              if (isset ($list[$i]) && $list[$i]->slug != 'uncategorized' )
                echo "<li><a href=\"".get_term_link( $list[$i]->term_id, $taxonomy_type )."\">".$list[$i]->name."</a></li>";
              else
                break;
            }
            ?>
        </ul>
    </div><!-- #main -->
    <?php if ( $list ) { ?>
    <div class="kajuzi-taxonomy-card-more-link">
            <a href="<?php echo $more_link ?>"><?php echo $more_text ?></a>
    </div>
    <?php } ?>

  </div><!-- #primary -->
</div>

<?php
}

add_shortcode('kajuzigrid_sc_post_grid', 'kajuzigrid_sc_post_grid');

add_shortcode('kajuzi_pruduct_taxonomies', 'kajuzi_pruduct_taxonomies');
